import React from 'react';
import { connect } from 'react-redux';
import { getLoggedInUser } from '../../store/selectors';
import { Header, Navigation, HeaderButtonContainer, HeaderButton, Logo, Flex1 } from './styles';
import AdministrationPageStyle from './styles';
import HeaderMenu from './Header/index';

class AdministrationPage extends React.Component {
  render() {
    const { user } = this.props;
    return(
      <React.Fragment>
        <AdministrationPageStyle />
        <Header>
          <Navigation>
            <HeaderButtonContainer>
              <HeaderButton className="btn">
                  <i className="fa fa-bars" style={{color: 'white'}}></i>
              </HeaderButton>
            </HeaderButtonContainer>
            <Logo><span>Sistema de Administração Belamar Hotel</span></Logo>
            <Flex1 />
            {/* <HeaderButtonContainer>
                <HeaderButton className="btn">
                  <i className="fas fa-user" style={{color: 'white'}}></i>
                </HeaderButton>
            </HeaderButtonContainer> */}
            <HeaderMenu user={user}/>
          </Navigation>
        </Header>
        <main style={{margin: '120px 0 0 10px'}}>
          <p>Content TDB ...</p>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: getLoggedInUser(state)
});

export default connect(mapStateToProps, null)(AdministrationPage);
