import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import styled from 'styled-components';

const NavBarHeaderContainer = styled.div`
  .navbar {
    background-color: #17a2b8;
    font-size: 13px;
  }
  .navbar-brand {
    font-size: 13px;
  }
`;

const HeaderMenu = props => {
  return(
    <NavBarHeaderContainer>
      <Navbar>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            Logado como: <a href="#login">{props.user.name}</a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
    </NavBarHeaderContainer>
  )
}

export default HeaderMenu;
