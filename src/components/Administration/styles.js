import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';

export const Flex1 = styled.div`
  flex: 1;
`;

export const Header = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background-color: #17a2b8;
  height: 60px;
`;

export const Navigation = styled.nav`
  display: flex;
  height: 100%;
  align-items: center;
  padding: 0 1rem;
`
export const Logo = styled.div`
  span {
    color: white;
    font-size: 15px;
    padding: 0 1rem;

    @media(max-width: 375px) {
      font-size: 12px;
    }
  }
`;

export const HeaderButtonContainer = styled.div`
  &:hover {
    background-color: transparent;
    opacity: 0.5;
  }
`
export const HeaderButton = styled.button`
  border: 1px white solid !important;
  font-size: 11px !important;
`;

export default createGlobalStyle`
 * {
        margin: 0;
        padding: 0;
        outline: 0;
    }

    html, body, #root {
        height: 100%;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-size: 15px;
        background-color: #dedede;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
    }
`;
