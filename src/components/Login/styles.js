import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';
import belamar from '../../assets/belamar.jpeg';
import Form from 'react-bootstrap/Form';

export const FormContainer = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormContent = styled.div`
  width: 100%;
  max-width: 400px;
  margin: 30px;
  background: #fff;
  border-radius: 4px;
  padding: 20px;

  .form-group {
    margin-bottom: 0;
  }
`;

export const StyledFormGroup = styled(Form.Group)`
  input {
    display: flex;
    position: relative;
    padding-left: 35px;
  }

  i {
    position: relative;
    left: 12px;
    bottom: 24px;
  }
`
export const StyledErrorMessage = styled.span`
  font-size: 12px;
  position: relative;
  color: red;
  right: 15px;
`;

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: 0;
    }

    html, body, #root {
        height: 100%;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-size: 12px;
        background: url(${belamar});
        background-size: cover;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
    }
`;
