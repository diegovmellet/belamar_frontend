import { SET_AUTHENTICATION_SUCCESS, SET_AUTHENTICATION_ERROR } from '../actions/index';

const INITIAL_STATE = { user: {}, hasError: false }

export const userReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SET_AUTHENTICATION_SUCCESS:
      const { user } = action.data;

      return { ...state, user: user, hasError: false };

    case SET_AUTHENTICATION_ERROR:
      return { ...state, hasError: action.payload}

    default:
      return state;
  }
};

