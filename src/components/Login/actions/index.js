import { push } from 'connected-react-router';
export const SET_AUTHENTICATION_SUCCESS = 'SET_AUTHENTICATION_SUCCESS';
export const SET_AUTHENTICATION_ERROR = 'SET_AUTHENTICATION_ERROR';

export const setAuthenticationSucess = data => {
  localStorage.setItem('jwtToken', 'Bearer ' + data.token);
  return { type: SET_AUTHENTICATION_SUCCESS, data };
}

export const setAuthenticationError = () => ({ type: SET_AUTHENTICATION_ERROR, payload: true });
export const redirectToAdministrationPage = () => dispatch => dispatch(push('/administration'));

