import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { authenticateUser } from '../../services/index';
import * as Actions  from './actions/index';

import { FormContainer, FormContent, StyledFormGroup, StyledErrorMessage } from './styles';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import LoginPageStyle from './styles';

const LoginPage = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const { hasError } = props;

  const handleClick = async event => {
    event.preventDefault();

    setIsLoading(true);
    const response = await authenticateUser({ email, password });

    if(response.ok) {
      props.setAuthenticationSucess(response.data);
      props.redirectToAdministrationPage();
    } else {
      props.setAuthenticationError();
    }

    setIsLoading(false);
  }

  return (
      <React.Fragment>
      <LoginPageStyle />
        <FormContainer>
            <FormContent>
              <Form>
                  <StyledFormGroup controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="email"
                      value={email}
                      isInvalid={hasError}
                      name="email"
                      placeholder="Digite seu email"
                      onChange={ event => setEmail(event.target.value) }
                    />
                    <i className="far fa-envelope"></i>
                    { hasError ? <StyledErrorMessage> Email ou Senha inválidos</StyledErrorMessage> : null }
                  </StyledFormGroup>

                  <StyledFormGroup controlId="password">
                    <Form.Label>Senha</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      value={password}
                      isInvalid={hasError}
                      placeholder="Digite sua senha"
                      onChange={ event => setPassword(event.target.value) }
                    />
                    <i className="fas fa-key"></i>
                    { hasError ? <StyledErrorMessage> Email ou Senha inválidos</StyledErrorMessage> : null }
                  </StyledFormGroup>

                  { isLoading
                    ? <Button variant="info" size="lg" block disabled>
                        <Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true"/>
                      </Button>
                    : <Button
                        variant="info"
                        type="submit"
                        size="lg"
                        block
                        onClick={handleClick}> Entrar
                      </Button>
                  }
              </Form>
            </FormContent>
          </FormContainer>
        </React.Fragment>
  );
}

const mapDispatchToProps = dispatch => bindActionCreators(Actions, dispatch);
const mapStateToProps = state => ({ hasError: state.userReducer.hasError });
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
