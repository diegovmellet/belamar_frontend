import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import history from '../routes/history';

import { userReducer } from '../components/Login/reducer/index';

const middlewares = [ thunk, routerMiddleware(history) ];

const createRootReducer = history => combineReducers({
  router: connectRouter(history),
  userReducer: userReducer
});

const store = createStore(
  createRootReducer(history),
  compose(applyMiddleware(logger, ...middlewares))
);

export default store;
