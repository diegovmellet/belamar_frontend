import { create } from 'apisauce';

const api = create({
    baseURL: 'http://belamar-be.herokuapp.com/'
});

export const authenticateUser = data => api.post('auth/authenticate', data);
