import React from 'react';
import Routes from './routes/index'
import { Provider } from 'react-redux';
import store from './store/index';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

const App = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);

export default App;
