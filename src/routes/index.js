import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import Administration from '../components/Administration';
import LoginPage from '../components/Login';

import history from './history'

const Routes = () => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact path="/" component={LoginPage} />
      <Route path="/administration" component={Administration} />
    </Switch>
  </ConnectedRouter>
);

export default Routes;
